# HttpTest
本软件由Java语言实现，跨平台运行的接口测试工具，支持各种API接口调试。

支持Get、Post、Put、Delete、Options等多种请求方法。

软件自带模拟各种浏览器、手机浏览器、微信浏览器，也可以在自定义请求头中设置自定义请求头。

例子：https://payjs.cn/help/api-lie-biao/sao-ma-zhi-fu.html
个人微信支付接口测试

![输入图片说明](https://gitee.com/uploads/images/2018/0404/181612_b9976174_383838.png "屏幕截图.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0404/181930_054a5a38_383838.png "屏幕截图.png")

获取响应
![输入图片说明](https://gitee.com/uploads/images/2018/0404/182128_a13f3ae2_383838.png "屏幕截图.png")